const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');
const CompWorkouts = require('../models/completedworkouts');
var Promise = require('promise');

const WorkoutsSchema = mongoose.Schema({
    activityId: {
        type: Number,
    },
    activityParentId: {
        type: Number
    },
    calories: {
        type: Number
    },
    description: {
        type: String
    },
    distance: {
        type: String
    },
    duration: {
        type: Number
    },
    hasStartTime: {
        type: Boolean
    },
    isFavorite: {
        type: Boolean
    },
    logId: {
        type: Number
    },
    name: {
        type: String
    },
    startTime: {
        type: String
    },
    dateComepleted: {
        type: Date
    },
    steps: {
        type: Number
    }

});

const Workouts = module.exports = mongoose.model('activites', WorkoutsSchema, 'activites');

