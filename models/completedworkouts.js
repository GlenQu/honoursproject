const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const config = require('../config/database');


const CompletedWorkoutsSchema = mongoose.Schema({
    activityId: {
        type: Number,
    },
});

const CompletedWorkouts = module.exports = mongoose.model('CompletedWorkouts', CompletedWorkoutsSchema);

module.exports.getAllCompWorkouts = function() {
    CompletedWorkouts.find((data, err) => {
        if(err)
            console.log('Error');
    });
}

module.exports.saveActivity = function(workout, callback) {
    let activityId = new CompletedWorkouts();
    activityId.activityId = workout.logId;
    activityId.save();
}