const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const Workouts = require('../models/workouts');
const User = require('../models/user');
const CompWorkouts = require('../models/completedworkouts');
var Promise = require('promise');

const config = require('../config/database');


router.get('/completed', passport.authenticate('jwt', { session: false }), (req, res, next) => {

    // workoutsArray = [];
    // completedworkouts = [];
    // sortedWorkoutList = [];


    // Workouts.find((err, data) => {
    //     workoutsArray = data;
    //     CompWorkouts.find((err, data) => {
    //         completedworkouts = data

    //         var filteredArray  = workoutsArray.filter(function(array_el){
    //             return completedworkouts.filter(function(anotherOne_el){
    //                return anotherOne_el.activityId == array_el.activityId;
    //             }).length == 0
    //          });

    //         if (completedworkouts.length == 0) {
    //             res.json({ data: workoutsArray });
    //         } else {
    //             res.json({ data: filteredArray });
    //         }
    //     });
    // });
    CompWorkouts.find((err, data) => {
        if (err) throw err;
        res.json({data: data});
    });

});


router.post('/save', passport.authenticate('jwt', { session: false }), (req, res, next) => {
    user = req.body.user;
    workout = req.body.workout;
    workout.dateCompleted = new Date();
    console.log(workout);

    User.saveWorkoutToUser(user, workout, (err, data) => {
        if (err) {
            res.json({ success: false, msg: 'Failed to add workout.' });
        } else {
            res.json({ success: true, msg: 'Workout added.' });
        }
    });

    CompWorkouts.saveActivity(workout, (err, data) => {
        if (err) {
            res.json({ success: false, msg: 'Failed to add workout.' });
        } else {
            res.json({ success: true, msg: 'Workout added.' });
        }
    });

});

module.exports = router;