import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router } from '@angular/router';
import { environment } from '../../../environments/environment'


@Component({
  selector: 'app-add-workout',
  templateUrl: './add-workout.component.html',
  styleUrls: ['./add-workout.component.css']
})
export class AddWorkoutComponent implements OnInit {

  workouts = [];
  userId: string;
  oauthToken: string;
  angularUrl: string;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit() {
    this.angularUrl = 'https://www.fitbit.com/oauth2/authorize?response_type=token&client_id=22CXVG&redirect_uri=' + environment.angularLocal + '&scope=activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800';


    var url = window.location.href;

    if (url.split("#")[1] != undefined) {
      this.getCredentials(url);
    }

    this.authService.getUnloggedWorkouts(this.userId, this.oauthToken).subscribe(data => {
      this.authService.getCompletedWorkouts().subscribe(data1 => {
        let workoutsArray = [];
        workoutsArray = data.activities; 
        let completedWorkouts = [];
        completedWorkouts = data1.data;
        this.workouts  = workoutsArray.filter(function(array_el){
                      return completedWorkouts.filter(function(anotherOne_el){
                         return anotherOne_el.activityId == array_el.logId;
                      }).length == 0
                   });
      })

    }, err => {
      console.log(err);
      return false;
    });

  }

  submitWorkout(data) {
    const user = JSON.parse(localStorage.getItem('user'));
    this.authService.saveWorkout(user, data).subscribe(data => {
      console.log(data);
      this.router.navigate(['/dashboard']);
    });
  }

  getCredentials(url) {
    this.oauthToken = url.split("#")[1].split("=")[1].split("&")[0];
    this.userId = url.split("#")[1].split("=")[2].split("&")[0];
    localStorage.setItem('fitbitid', this.userId);
    localStorage.setItem('oauth', this.oauthToken);
  }

  msToTime(s) {
    var ms = s % 1000;
    s = (s - ms) / 1000;
    var secs = s % 60;
    s = (s - secs) / 60;
    var mins = s % 60;
    var hrs = (s - mins) / 60;

    if(hrs.toString().length < 2){
      var hrsStr = '0'+hrs.toString();
    }
    else{
      var hrsStr = hrs.toString();
    }

    if(mins.toString().length < 2){
      var minsStr = '0'+mins.toString();
    }
    else{
      var minsStr = mins.toString();
    }

    if(secs.toString().length < 2){
      var secsStr = '0'+secs.toString();
    }
    else{
      var secsStr = secs.toString();
    }

    return hrsStr + ':' + minsStr + ':' + secsStr;
  }

}
