import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Chart } from 'chart.js';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  constructor(private authService: AuthService) { }

  activitiesData = [];
  dateData = [];
  chart = []
  workoutsAgreggated = [];
  currentWorkoutCount: Number;
  previousWorkoutCount: Number

  ngOnInit() {
    this.authService.getProfile().subscribe(data => {
      this.currentWorkoutCount = this.returnCurrentWeek(data.user.activities).length;
      this.previousWorkoutCount = this.returnPreviousWeek(data.user.activities).length;
      this.workoutsAgreggated = this.returnPrevious7Days(data.user.activities);
      this.workoutsAgreggated = this.sumWorkoutsPerDate(this.workoutsAgreggated);
      this.returnPreviousWeek(data.user.activities);
      this.workoutsAgreggated.sort(function (a, b) {
        a = new Date(a.name);
        b = new Date(b.name);
        return a - b
      });
      this.workoutsAgreggated.forEach(element => {
        this.dateData.push(element.name);
        this.activitiesData.push(element.count);
      })
      this.chart = new Chart('canvas', {
        type: 'bar',
        data: {
          labels: this.dateData,
          datasets: [
            {
              label: "Activities Completed",
              data: this.activitiesData,
              backgroundColor: "#1a5dc9"
            }
          ]
        },
        options: {
          scales: {
            yAxes: [{
              display: true,
              ticks: {
                beginAtZero: true,
                stepSize: 1
              }
            }]
          }
        }
      });
    })
  }

  sumWorkoutsPerDate(b) {
    var counts = b.reduce((p, c) => {
      var name = c.startTime.slice(0,10);
      if (!p.hasOwnProperty(name)) {
        p[name] = 0;
      }
      p[name]++;
      return p;
    }, {});

    var countsExtended = Object.keys(counts).map(k => {
      return { name: k, count: counts[k] };
    });

    return countsExtended;
  }

  returnPrevious7Days(array) { 
    array.forEach((element, i) => {
      var dateToBeChecked = new Date(element.startTime.slice(0,10))
      if (new Date(element.startTime.slice(0,10)) < new Date(new Date().setDate(new Date().getDate() - 7))) {
        array.slice(i);
      }
    });
    return array;
  }

  returnCurrentWeek(array) { 
    let date = this.getMonday(new Date());
    let newArray = []
    array.forEach((element, i) => {
      var dateToBeChecked = new Date(element.startTime.slice(0,10))
      if (new Date(element.startTime.slice(0,10)) < date) {
        newArray.push(element);
      }
    });
    return newArray;
  }

  returnPreviousWeek(array) { 
    let prevMonday = this.getMonday(new Date());
    prevMonday.setDate(prevMonday.getDate() - 7);
    let sevenDays = new Date(prevMonday.getFullYear(), prevMonday.getMonth(), prevMonday.getDate()+6);
    let newArray = [];

    array.forEach((element, i) => {
      var dateToBeChecked = new Date(element.startTime.slice(0,10))
      
      if (dateToBeChecked > prevMonday && dateToBeChecked < sevenDays) {
        newArray.push(element);
      }
    });
    return newArray;
  }

  getMonday(d) {
    d = new Date(d);
    var day = d.getDay(),
        diff = d.getDate() - day + (day == 0 ? -6:1); // adjust when day is sunday
    return new Date(d.setDate(diff));
  }
}