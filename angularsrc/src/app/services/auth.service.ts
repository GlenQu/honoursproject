import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map'; 
import { tokenNotExpired } from 'angular2-jwt';
import { environment } from '../../environments/environment';

@Injectable()
export class AuthService {

  authToken: any;
  user: any;
  
  constructor(private http: Http) { }

  registerUser(user){
    let headers = new Headers();
    headers.append('ContentType', 'application/json');
    return this.http.post(environment.url+'/users/register', user, {headers: headers})
      .map(res => res.json());
  }

  authenticateUser(user){
    let headers = new Headers();
    headers.append('ContentType', 'application/json');
    return this.http.post(environment.url+'/users/authenticate', user, {headers: headers})
      .map(res => res.json());
  }

  loggedIn(){
    return tokenNotExpired('id_token');
  }

  getProfile(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('ContentType', 'application/json');
    return this.http.get(environment.url+'/users/profile', {headers: headers})
      .map(res => res.json());
  }

  getUnloggedWorkouts(userId, oauth){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', 'Bearer '+oauth);
    return this.http.get('https://api.fitbit.com/1/user/-/activities/list.json?beforeDate=2018-08-01&sort=desc&limit=100&offset=0', {headers: headers})
      .map(res => res.json());
  }

  getCompletedWorkouts(){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('ContentType', 'application/json');
    return this.http.get(environment.url+'/workouts/completed',  {headers: headers})
      .map(res => res.json());
  }

  saveWorkout(user, workout){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('ContentType', 'application/json');
    return this.http.post(environment.url+'/workouts/save',{workout, user},  {headers: headers})
      .map(res => res.json());
  }

  storeUserData(token, user){
    localStorage.setItem('id_token', token);
    localStorage.setItem('user', JSON.stringify(user));
    this.authToken = token;
    this.user = user;
  }

  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  logout(){
    this.authToken= null; 
    this.user = null;
    localStorage.clear();
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}

}
