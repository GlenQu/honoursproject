export const environment = {
  production: true,
  url: 'https://glen-hons.eu-gb.mybluemix.net',
  angularLocal: 'https%3A%2F%2Fglen-hons.eu-gb.mybluemix.net%2Faddworkout'
};
